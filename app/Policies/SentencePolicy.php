<?php

namespace App\Policies;

use App\User;
use App\Sentence;
use Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class SentencePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\User $user
     * @param Sentence $sentence
     * @return mixed
     */
    public function view(User $user, Sentence $sentence)
    {
        return $sentence->user_id === $user->id || $user->admin || $sentence->user_id === null;
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\User $user
     * @param Sentence $sentence
     * @return mixed
     */
    public function update(User $user, Sentence $sentence)
    {
        return $sentence->user_id === $user->id || $user->admin;
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\User $user
     * @param Sentence $sentence
     * @return mixed
     */
    public function delete(User $user, Sentence $sentence)
    {
        return $sentence->user_id === $user->id || $user->admin;
    }
}
