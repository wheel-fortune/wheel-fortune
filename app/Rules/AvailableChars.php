<?php

namespace App\Rules;

use App\Sentence;
use App\Services\GameService;
use Illuminate\Contracts\Validation\Rule;

class AvailableChars implements Rule
{
    private $gameService;

    /**
     * Create a new rule instance.
     *
     * @param GameService $gameService
     */
    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //dd($value);
        return preg_match_all('/(^[a-złąśżźćńęó,\s]{1,}$)/iu', $value) && !preg_match('/[x]/i', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Użyto nieprawidłowych znaków. Dostępne znaki to litery polskiego alfabetu oraz przecinek.';
    }
}
