<?php

namespace App\Rules;

use App\Sentence;
use App\Services\GameService;
use Illuminate\Contracts\Validation\Rule;

class SentenceLength implements Rule
{

    private $gameService;
    private $maxLength;

    /**
     * Create a new rule instance.
     *
     * @param GameService $gameService
     */
    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
        $this->maxLength = $gameService->maxLength;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $rows = $this->gameService->createTable(new Sentence(['text' => $value]));
        foreach ($rows as $row) {
            if (count($row) > $this->maxLength) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Długość podanego zdania nie mieści się w polu gry! Skróć swoje zdanie lub nie używaj słów dłuższych niż '.$this->maxLength.' znaków.';
    }
}
