<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Sentence
 *
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sentence byUser(\App\User $user = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sentence newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sentence newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sentence query()
 * @mixin \Eloquent
 */
class Sentence extends Model
{
    protected $fillable = ['text', 'category_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Scope a query to only user's sentences or global
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  User|null $user
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByUser($query, User $user = null): Builder
    {
        if (!$user) {
            return $query->whereNull('user_id');
        }
        return $query->where('user_id', $user->id)->orWhereNull('user_id');
    }

    public function setTextAttribute($value)
    {
        $this->attributes['text'] = mb_strtoupper($value, 'UTF-8');
    }
}
