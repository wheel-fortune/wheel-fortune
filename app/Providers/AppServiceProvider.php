<?php

namespace App\Providers;

use Auth;
use Collective\Html\FormBuilder;
use Form;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\HtmlString;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Form::macro('error', function ($name) {
            $name   = FormBuilder::transformKey($name);
            $errors = Form::getSessionStore()->get('errors');;
            if (!empty($errors) && $errors->has($name)) {
                return new HtmlString('<label class="text-danger" for="'. $name .'">'. $errors->first($name) .'</span>');
            }
            return null;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
