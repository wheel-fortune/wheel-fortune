<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\SentenceValidation;
use App\Sentence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SentenceController extends Controller
{
    private function getSentences(){
        if (Auth::user()->admin){
            return Sentence::get();
        }
        return Sentence::where('user_id', Auth::user()->id)->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sentence.index')->with('alert')->with('sentences', $this->getSentences());
    }

    public function global()
    {
        return view('sentence.global')->with('alert')->with('sentences', Sentence::where('user_id')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sentence.form')->with('sentence')->with('categories', Category::all()->pluck('name', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SentenceValidation $request)
    {
        $sentence = new Sentence($request->all());
        $sentence->text = $request->text;
        if (Auth::user()->admin && $request->global) {
            $sentence->user_id = null;
        } else {
            $sentence->user()->associate(Auth::user());
        }
        $sentence->category()->associate($request->category_id);
        $sentence->save();
        return view('sentence.index')->with('alert', 'Dodano nowe zdanie.')->with('sentences', $this->getSentences());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sentence  $sentence
     * @return \Illuminate\Http\Response
     */
    public function show(Sentence $sentence)
    {
        return redirect(route('sentence.index'))->with('alert')->with('sentences', $this->getSentences());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sentence $sentence
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Sentence $sentence)
    {
        $this->authorize($sentence);
        return view('sentence.form')->with('sentence', $sentence)->with('categories', Category::all()->pluck('name', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Sentence $sentence
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(SentenceValidation $request, Sentence $sentence)
    {
        //dd($sentence);
        $this->authorize($sentence);
        $sentence->fill($request->all());
        if (!Auth::user()->admin || !$request->get('global')) {
            $sentence->user()->associate(Auth::user());
        }
        $sentence->save();
        return view('sentence.index')->with('alert', 'Zedytowano zdanie')->with('sentences', $this->getSentences());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sentence $sentence
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Sentence $sentence)
    {
        $this->authorize($sentence);
        $sentence->delete();
        return view('sentence.index')->with('alert', 'Usunięto zdanie')->with('sentences', $this->getSentences());
    }
}
