<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryValidation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('category.index')->with('alert')->with('categories', Category::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.form')->with('category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryValidation $request)
    {
        $category = new Category($request->all());
        $category->name = $request->name;
        $category->save();
        return view('category.index')->with('alert', 'Dodano nową kategorię.')->with('categories', Category::get());
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return redirect(route('sentence.index'))->with('alert')->with('categories', Category::get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sentence $sentence
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Category $category)
    {
        return view('category.form')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryValidation $request
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryValidation $request, Category $category)
    {
        $category->fill($request->all());
        $category->save();
        return view('category.index')->with('alert', 'Zedytowano kategorię')->with('categories', Category::get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return view('category.index')->with('alert', 'Usunięto kategorię')->with('categories', Category::get());
    }
}
