<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Services\GameService;
use App\Sentence;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
    /**
     * @var GameService
     */
    private $gameService;

    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }

    public function init()
    {
        if (session()->has('game')){
            $sentence = Sentence::find(session('game')['sentence_id']);
        } else {
            $sentence = new Sentence();
            $sentence->text = 'KOŁO FORTUNY';
        }

        return view('welcome')
            ->with('gameTable', $this->gameService->createTable($sentence))
            ->with('letters', $this->gameService->createLetters());
    }

    public function start(Request $request)
    {
        $sentence = Sentence::byUser(Auth::user())->inRandomOrder()->first();
        $game = [
            'sentence_id' => $sentence->id,
            'selected_letters' => [],
            'toSelect' => $this->gameService->toSelect($sentence)
        ];
        $request->session()->put('game', $game);

        return redirect()->route('game');
    }

    public function clickLetter(Request $request): array
    {
        if (!isset(session('game')['sentence_id'])){
            return [
                'status' => 'no session'
            ];
        }
        $game = $request->session()->get('game');
        if (in_array($request->get('letter'), $game['selected_letters'], true) && $game['toSelect'] !== 0) {
            return [
                'status' => 'duplicate'
            ];
        }
        $game['selected_letters'][] = $request->get('letter');
        if ($game['toSelect'] > 0){
            $game['toSelect'] -= $request->get('found');
        }
        $request->session()->put('game', $game);
        if ($game['toSelect'] === 0) {
            return [
              'status' => 'endGame'
            ];
        }
        return [
            'status' => 'success'
        ];
    }

    public function forceWin(Request $request)
    {
        if (!$request->get('win')) {
            return [
                'status' => 'Unauthorized'
            ];
        }
        if (!isset(session('game')['sentence_id'])){
            return [
                'status' => 'no session'
            ];
        }
        $game = $request->session()->get('game');
        $game['toSelect'] = 0;
        $request->session()->put('game', $game);
        return [
            'status' => 'success'
        ];
    }

    public function theme(Request $request)
    {
        if (Auth::check()){
           $user = User::find(Auth::user()->id);
           $user->theme = $request->get('theme');
           $user->save();
        } else {
            $request->session()->put('theme', $request->get('theme'));
        }
        return [
            'status' => 'success. theme: ' . $request->get('theme')
        ];
    }
}
