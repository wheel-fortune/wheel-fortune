<?php

namespace App\Http\Requests;

use App\Rules\SentenceLength;
use App\Rules\AvailableChars;
use App\Sentence;
use App\Services\GameService;
use Illuminate\Foundation\Http\FormRequest;

class SentenceValidation extends FormRequest
{

    private $gameService;
    private $maxLength;

    public function __construct(GameService $gameService)
    {
        parent::__construct();
        $this->gameService = $gameService;
        $this->maxLength = $gameService->maxLength;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'text' => ['required', 'min:2', 'max:' . $this->maxLength * 4, new SentenceLength($this->gameService), new AvailableChars($this->gameService)]
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Pole nie może być puste.',
            'min' => 'Pole jest za krótkie.',
            'max' => 'Pole jest za długie.'
        ];
    }
}
