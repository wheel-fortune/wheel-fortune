<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends Model
{
    protected $fillable = ['name'];

    /**
     * @return BelongsToMany
     */
    public function sentence(): BelongsToMany
    {
        return $this->belongsToMany(Sentence::class);
    }
}