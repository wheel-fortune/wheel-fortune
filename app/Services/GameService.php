<?php

namespace App\Services;

use App\Sentence;
use Illuminate\Support\Str;

class GameService extends BaseService
{
    /**
     * @var int Game lines for sentence
     */
    public $maxRows = 4;

    /**
     * @var int Letters per line
     */
    public $maxLength = 12;

    /**
     *  Create an array with polish alphabet
     *
     *  @return  array
     */
    public function createLetters(): array
    {
        setlocale(LC_ALL, 'pl_PL', 'pl', 'Polish_Poland.28592');

        $letters = range('A', 'Z');
        $letters = array_diff($letters, ['X', 'V', 'Q']);
        array_push($letters, 'Ą', 'Ż', 'Ź', 'Ć', 'Ń', 'Ł', 'Ę', 'Ó', 'Ś');
        sort($letters, SORT_LOCALE_STRING);
        return $letters;
    }

    /**
     * @param  $string
     * @return array
     */
    private function mbStringToArray($string): array
    {
        $array = [];
        $strlen = Str::length($string);
        while ($strlen) {
            $array[] = Str::substr($string, 0, 1);
            $string  = Str::substr($string, 1, $strlen);
            $strlen  = Str::length($string);
        }

        return $array;
    }

    /**
     * @param  Sentence  $sentence
     * @return int
     */
    public function toSelect(Sentence $sentence): int
    {
        $sentenceArray = $this->mbStringToArray($sentence->text);
        foreach (array_keys($sentenceArray, '') as $key) {
            unset($sentenceArray[$key]);
        }
        foreach (array_keys($sentenceArray, ' ') as $key) {
            unset($sentenceArray[$key]);
        }
        foreach (array_keys($sentenceArray, ',') as $key) {
            unset($sentenceArray[$key]);
        }

        return count($sentenceArray);
    }

    /**
     * @param  Sentence  $sentence
     * @return array
     */
    public function createTable(Sentence $sentence): array
    {
        $currentRow = $currentPosition = 0;
        $availableLettersInRow = $this->maxLength;

        $rows = array_fill(0, $this->maxRows, array_fill(0, $this->maxLength, ' '));

        foreach (explode(' ', $sentence->text) as $word) {
            $wordLength = Str::length($word);
            //if($word == "dzieje") dd($availableLettersInRow);
            if ($wordLength > $availableLettersInRow) {
                $currentRow++;
                $availableLettersInRow = $this->maxLength;
                $currentPosition = 0;
            }
            for ($i = 0; $i <= $wordLength; $i++) {
                $availableLettersInRow -= Str::length(Str::substr($word, $i, 1));
                if (Str::substr($word, $i, 1) === ',') {
                    $rows[$currentRow][$currentPosition] = ',';
                    $currentPosition++;
                    break;
                }
                if ($i === $wordLength) {
                    if ($currentPosition !== $this->maxLength) {
                        $rows[$currentRow][$currentPosition] = '';
                    }
                    $currentPosition++;
                    $availableLettersInRow--;
                    continue;
                }
                $rows[$currentRow][$currentPosition] = Str::substr($word, $i, 1);
                $currentPosition++;
            }
        }

        $i = 0;
        foreach ($rows as $row) {
            $spaces = 0;
            foreach ($row as $letter) {
                if ($letter === ' ') {
                    $spaces++;
                }
            }
            if ($spaces > 1) {
                $moveBy = round($spaces / 2);
                //if($i == 1) dd($moveBy);
                $newRow = [$this->maxLength];
                for ($x = 0; $x < $this->maxLength; $x++) {
                    $newRow[$x] = ' ';
                }
                for ($ii = 0; $ii < count($row) - $moveBy; $ii++) {
                    $newRow[$ii + $moveBy] = $row[$ii];
                }
                $rows[$i] = $newRow;
            }
            $i++;
        }

        $emptyRows = [true, true, true, true, 4];
        $i = 0;
        foreach ($rows as $row) {
            foreach ($row as $letter) {
                if ($letter !== ' ') {
                    $emptyRows[$i] = false;
                    $emptyRows[4]--;
                    break;
                }
            }
            $i++;
        }

        if ($emptyRows[4] >= 2) {
            $emptyRow = $rows[3];
            $rows[2] = $rows[1];
            $rows[1] = $rows[0];
            $rows[0] = $emptyRow;
        }
        //dd($rows);
        return $rows;
    }
}