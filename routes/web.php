<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GameController@init')->name('game');

Auth::routes(['verify' => true]);

Route::get('/sentence', 'HomeController@index')->name('home')->middleware('verified');

Route::resource('/sentence', 'SentenceController')->middleware('verified');

Route::get('/global-sentence', 'SentenceController@global')->name('sentence.global')->middleware('verified');

Route::resource('/category', 'CategoryController')->middleware('admin');

Route::post('/start-game', 'GameController@start')->name('game.start');

Route::post('/click-letter', 'GameController@clickLetter')->name('game.letter');

Route::post('/change-theme', 'GameController@theme')->name('game.theme');

Route::post('/force-win', 'GameController@forceWin')->name('game.win');

Route::get('/about', 'HomeController@about')->name('about');

Route::get('/read-me', 'HomeController@readMe')->name('read.me');