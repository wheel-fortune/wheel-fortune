@extends('home')

@section('main')
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if($alert !== null)
                <div class="alert alert-danger" role="alert">
                    {{ $alert }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                     <span class="header-title">Wszystkie kategorie</span><span class="float-right"><a href="{{ route('category.create') }}"><button class="btn btn-success btn-small"><i class="fas fa-plus-circle"></i> Dodaj</button></a></span>
                </div>
                <div class="card-body">
                    <table class="table panel-table">
                        @foreach($categories as $category)
                            <tr>
                                <td>
                                    <span class="float-left">{{ $category->name }}</span>
                                    {{ Form::open(['route' => ['category.destroy', $category], 'method' => 'delete']) }}
                                        <span class="float-right">
                                            <a href="{{ route('category.edit', $category) }}">
                                                <span class="btn btn-link"><i class="fas fa-edit fa-lg text-warning"></i></span>
                                            </a>
                                            <button type="submit" class="btn btn-link"><i class="fas fa-trash-alt fa-lg text-danger ml-3 "></i></button>
                                        </span>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection