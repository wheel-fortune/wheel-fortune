@extends('home')

@section('main')
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if ($errors->any())
                <div class="alert alert-danger">
                        Formularz zawiera błędy!
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <b>{{ $category ? 'Edytuj kategorię' : 'Dodaj nową kategorię' }}</b>
                </div>
                <div class="card-body">
                    {{ Form::model($category, ['route' => ['category.'. ( $category ? 'update':'store'), $category], 'method' => ( $category ? 'put' : 'post')])}}
                    <div class="form-group">
                        {{ Form::label('name', 'Nazwa kategorii: ') }}
                        {{ Form::text('name', $category ? $category->name : null, ['class' => 'form-control '. ($errors->has('name') ? 'is-invalid' : ''), 'required' => 'required']) }}
                        {{ Form::error('name') }}
                    </div>
                    {{ Form::submit('Dodaj', ['class' => 'btn btn-success']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection