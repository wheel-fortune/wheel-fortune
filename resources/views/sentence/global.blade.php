@extends('home')

@section('main')
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if($alert !== null)
                <div class="alert alert-danger" role="alert">
                    {{ $alert }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <span class="header-title">Globalne Hasła</span><span class="float-right"></span>
                </div>
                <div class="card-body">
                    <table class="table panel-table">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nazwa</th>
                            <th scope="col">Kategoria</th>
                        </tr>
                        </thead>
                        @foreach($sentences as $sentence)
                            <tr>
                                <td  data-label=""><b>#{{$sentence->id }}</b></td>
                                <td data-label="Nazwa">{{ $sentence->text }}</td>
                                <td data-label="Kategoria" ><span class="mybadge bg-info">{{ $sentence->category->name }}</span></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection