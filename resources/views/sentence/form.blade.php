@extends('home')

@section('main')
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if ($errors->any())
                <div class="alert alert-danger">
                        Formularz zawiera błędy!
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <b>{{ $sentence ? 'Edytuj hasło' : 'Dodaj nowe hasło' }}</b>
                </div>
                <div class="card-body">
                    {{ Form::model($sentence, ['route' => ['sentence.'. ( $sentence ? 'update':'store'), $sentence], 'method' => ( $sentence ? 'put' : 'post')])}}
                    <div class="form-group">
                        {{ Form::label('text', 'Twoje zdanie: ') }}
                        {{ Form::text('text', $sentence ? $sentence->text : null, ['class' => 'form-control '. ($errors->has('text') ? 'is-invalid' : ''), 'required' => 'required']) }}
                        {{ Form::error('text') }}
                    </div>
                    @if(Auth::user()->admin)
                        <div class="form-group">
                            <div>
                                <label>
                                {{ Form::checkbox('global', $sentence, ['class' => 'form-control', 'required' => 'required']) }} Globalne zdanie
                                </label>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        Kategorie:<br>
                        {{ Form::select('category_id', $categories, $sentence ? $sentence->category_id : null, ['class' => 'form-control '. ($errors->has('category_id') ? 'is-invalid' : '')]) }}
                        {{ Form::error('category_id') }}
                    </div>
                    {{ Form::submit('Dodaj', ['class' => 'btn btn-success']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection