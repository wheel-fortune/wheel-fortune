@extends('home')

@section('main')
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if($alert !== null)
                <div class="alert alert-danger" role="alert">
                    {{ $alert }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <span class="header-title">{{ Auth::user()->admin ? 'Wszystkie Hasła' : 'Twoje Hasła' }}</span><span class="float-right"><a href="{{ route('sentence.create') }}"><button class="btn btn-success btn-small"><i class="fas fa-plus-circle"></i> Dodaj</button></a></span>
                </div>
                <div class="card-body">
                    <table class="table panel-table">
                        <thead>
                        <tr>
                            <th scope="col">Nazwa</th>
                            @if(Auth::user()->admin)
                            <th scope="col">Autor</th>
                            @endif
                            <th scope="col">Kategoria</th>
                            <th scope="col">Operacja</th>

                        </tr>
                        </thead>
                        @foreach($sentences as $sentence)
                            <tr>
                                <td data-label="Nazwa"><span class="float-left"><b>#{{$sentence->id }}</b></span> {{ $sentence->text }}</td>
                                @if(Auth::user()->admin)
                                    <td data-label="Autor">{!! $sentence->user_id === null ? '<span class="text-danger">Globalne</span>' : $sentence->user->name !!}</td>
                                @endif
                                <td data-label="Kategoria"><span class="mybadge bg-info">{{ $sentence->category === null ? 'Brak kategorii' : $sentence->category->name }}</span></td>
                                {{ Form::open(['route' => ['sentence.destroy', $sentence], 'method' => 'delete']) }}
                                <td data-label="Operacja" ><a href="{{ route('sentence.edit', $sentence) }}"><span class="btn btn-link"><i class="fas fa-edit fa-lg text-warning"></i></span></a>


                                    <button type="submit" class="btn btn-link"><i class="fas fa-trash-alt fa-lg text-danger ml-3 "></i></button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection