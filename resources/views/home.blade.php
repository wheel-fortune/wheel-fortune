<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css">
</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-dark" id="sidebar-wrapper">
        <div class="sidebar-heading" > <a href="{{ url('/') }}"><span class=" pb-2">Koło wiedzy</span></a> </div>
        <div class="list-group list-group-flush">
            <a href="#" class="list-group-item list-group-item-action bg-dark"><i class="fas fa-chart-line text-info"></i> <span class="ml-3">Panel</span></a>
            <a href="{{ route('sentence.index') }}" class="list-group-item list-group-item-action bg-dark {{ (request()->is('sentence/index*')) ? 'active' : 'test' }}"><i class="far fa-clipboard text-info"></i> <span class="ml-3">Twoje hasła</span></a>
            @can('create', \App\Category::class)
                <a href="{{ route('category.index') }}" class="list-group-item list-group-item-action bg-dark"><i class="far fa-bookmark text-info"></i> <span class="ml-3">Kategorie</span></a>
            @else
                <a href="{{ route('sentence.global') }}" class="list-group-item list-group-item-action bg-dark"><i class="far fa-bookmark text-info"></i> <span class="ml-3">Globalne hasła</span></a>
            @endcan
            <a href="{{ route('read.me') }}" class="list-group-item list-group-item-action bg-dark"><i class="fab fa-readme text-warning"></i> <span class="ml-3">Przeczytaj</span></a>
            <a href="{{ route('about') }}" class="list-group-item list-group-item-action bg-dark"><i class="far fa-user text-warning"></i> <span class="ml-3">O projekcie</span></a>
        </div>
        <a href="{{ url('/') }}"> <button class="btn btn-info btn-small ml-5 mt-5"><i class="fas fa-dice-five mr-2"></i>Powrót do gry</button></a>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="btn btn-primary" id="menu-toggle"><i class="fas fa-bars"></i></button>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item dropdown">
                        @guest
                        @else
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @can('create', \App\Category::class)
                                    <span class="badge badge-danger mr-3 p-2">Administrator</span><span class="badge badge-info p-2 "><i class="fas fa-user-alt fa-sm"></i> {{ Auth::user()->name }}  <i class="fas fa-caret-down ml-1"></i></span>
                                @else
                                    <span class="badge badge-info p-2"><i class="fas fa-user-alt fa-sm"></i> {{ Auth::user()->name }}  <i class="fas fa-caret-down ml-1"></i></span>
                                @endcan
                            </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Wyloguj') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                        @endguest
                </ul>
            </div>
        </nav>


        <div class="container-fluid">

            @yield('main')

        </div>

    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#toggle-arrow").toggleClass('fa-arrow-right');
    });


</script>

</body>

</html>
