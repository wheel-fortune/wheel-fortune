@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Potwierdź swój adres Email') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Nowy link został wysłany na twój email.') }}
                        </div>
                    @endif

                    {{ __('Przed kontynuowaniem, sprawdź twój email w celu weryfikacji.') }}
                    {{ __('Jeśli nie otrzymałeś wiadomości') }}, <a href="{{ route('verification.resend') }}">{{ __('kliknij tutaj aby wysłać ją ponownie') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
