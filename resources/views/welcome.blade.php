<!doctype html>
<html lang="{{ strtoupper(App::getLocale()) }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">

    <title>{{ config('app.name') }}</title>
</head>
<body class="@if(Auth::check()) {{ Auth::user()->theme }} @else {{ session('theme') ? session('theme') : 'theme-dark' }} @endif">
{{-- {{ $activeTheme }} Zmienna globalna, logikę do -> ViewComposerServiceProvider --}}

<nav class="navbar navbar-expand-lg navbar-dark navbar-custom">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
    </div>
    <button type="button" class="navbar-toggler ml-auto float-right" data-toggle="collapse" data-target="#mynavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="mynavbar">
        <ul class="navbar-nav ml-auto">

            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <button type="button" data-toggle="tooltip" data-placement="bottom" title="Menu gry" class="btn btn-outline-danger"><i class="fas fa-cog fa-lg"></i></button>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <button type="button" class="dropdown-item " disabled>
                        {{ session('game') === null ? 'Tutaj zobaczysz numer hasła' : 'Id aktualnego hasła: #'}}{{ session('game') !== null ? session('game')['sentence_id'] : ''}}
                    </button>
                    <button class="dropdown-item btn-outline-warning">
                        Zakończ grę
                    </button>
                    <button class="dropdown-item btn-outline-warning">
                        Losuj nowe hasło
                    </button>

                    <div class="dropdown-divider"></div>

                    <a href="{{ route('about') }}">
                        <button class="dropdown-item">
                            O projekcie
                        </button>
                    </a>
                    <a href="{{ route('read.me') }}">
                        <button class="dropdown-item">
                            Przeczytaj!
                        </button>
                    </a>
                    <button class="theme-switch dropdown-item btn-outline-warning">Ciemny motyw</button>
                    @guest
                    @else
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                            {{ __('Wyloguj') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                     @endguest
                </div>
            </li>
            @auth
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('sentence.index') }}">
                        <button type="button" data-toggle="tooltip" data-placement="bottom" title="Panel gry" class="btn btn-outline-warning"><i class="fas fa-user-alt fa-lg"></i></button>
                    </a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">
                        <button type="button" class="btn btn-outline-warning">Zaloguj</button>
                    </a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">
                            <button type="button" class="btn btn-outline-warning">Rejestracja</button>
                        </a>
                    </li>
                @endif
            @endauth
        </ul>
    </div>
</nav>

<div class="technical-break">Obróć urządzenie poziomo!<br><br><i class="fas fa-sync-alt fa-5x m-5"></i></div>
<div class="category text-center">
    <span class="align-content-center">
        @if(session('game'))
            @if(!\App\Sentence::find(session('game')['sentence_id'])->category()->exists())
                Brak kategorii
            @else
                {{ \App\Sentence::find(session('game')['sentence_id'])->category()->pluck('name')[0] }}
            @endif
        @else
            Rozpocznij grę
        @endif
    </span>
</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" >
            <div class="modal-body">
                <h1 class="text-center text-success"> <i class="fas fa-trophy fa-sm text-warning"></i> Wygrałeś! <i class="fas fa-trophy fa-sm text-warning"></i> </h1>
                <h4 class="text-center mt-3 mb-4">Odkryłeś każdą literę w haśle. Koniec gry.</h4>
            </div>
            <div class="text-center float-center mb-4">
                {{ Form::open(['url' => route('game.start')]) }}
                <button id="play" type="submit" name="start" class="btn btn-green" >Losuj nowe hasło</button>
                <button type="button" class="btn btn-red" data-dismiss="modal">Zamknij</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<div class="flex-container">
    <div class="flex-item">
        <table class="game-table">
            @foreach($gameTable as $row)
                <tr>
                    @if($loop->first)
                        <td class="invisible"></td>
                    @else
                        @if($loop->last)
                            <td class="invisible"></td>
                        @else
                            <td class="no-letter"></td>
                        @endif
                    @endif
                    @foreach($row as $letter)
                       @if($letter !== '' && $letter !== ' ')
                           @if($letter === ',')
                                <td class="has-letter" data-letter="{{ $letter }}">{{ $letter }}</td>
                           @else
                                @if(isset(session('game')['selected_letters']))
                                    @if(session('game')['toSelect'] === 0 || in_array($letter, session('game')['selected_letters'], true))
                                        <td class="has-letter yes" data-letter="{{ $letter }}">{{ $letter }}</td>
                                    @else
                                        <td class="has-letter hide-letter" data-letter="{{ $letter }}">{{ $letter }}</td>
                                    @endif
                                @else
                                    <td class="has-letter no" data-letter="{{ $letter }}">{{ $letter }}</td>
                                @endif
                           @endif
                       @else
                           <td class="no-letter"></td>
                       @endif
                    @endforeach
                    @if($loop->first)
                        <td class="invisible"></td>
                    @else
                        @if($loop->last)
                            <td class="invisible"></td>
                        @else
                            <td class="no-letter"></td>
                        @endif
                    @endif
                </tr>
            @endforeach
        </table>
    </div>
    <div class="buttons">
        <div class="notification">Litera nie pasuje do hasła</div>
    </div>
    <div class="flex-item-letters-table lettersTable">
        <div class="letters">
            @foreach($letters as $letter)
                @if($loop->iteration > round($loop->count / 2))
                    @continue
                @endif
                <div class="letter-button" data-letter="{{ $letter }}">{{ $letter }}</div>
            @endforeach
        </div>
        <div class="letters">
            @foreach($letters as $letter)
                @if($loop->iteration <= round($loop->count / 2))
                    @continue
                @endif
                <div class="letter-button" data-letter="{{ $letter }}">{{ $letter }}</div>
            @endforeach
        </div>
    </div>

    <div class="buttons">
        {{ Form::open(['url' => route('game.start')]) }}
        <button id="play" type="submit" name="start" class="btn btn-green p-3 ml-3"><i class="fas fa-dice fa-lg"></i>&nbsp Zagraj
        </button>
        <button type="button" id="play" class="btn btn-yellow p-3 ml-3 force-win"><i class="fas fa-smile-beam fa-lg"> </i>
            &nbsp Odkryj hasło lub Zakończ grę
        </button>
        {{ Form::close() }}
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<!--<script>
    let $playButton = $("#play")

    showPlayButton()
    function showPlayButton() {
        $playButton.fadeOut("fast").off()
    }
</script>

<script>
$("#play").on('click', function(){
  $(this).animate({
      opacity: 0,
  }, 1500);
});
</script>
-->
<script>

    let sounds = {
        fail: new Audio('{{ asset('sounds/fail.mp3') }}'),
        good: new Audio('{{ asset('sounds/good.mp3') }}'),
        letterFlip: new Audio('{{ asset('sounds/letter-flip.mp3') }}')
    };

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.letter-button').click(function (e) {
            let letter = $(this).data('letter');
            let found  = $('tr').find(`[data-letter='${letter}']`);
            if (found.length > 0) {
                //sounds.letterFlip.play();
                found.each(function () {
                    $(this).removeClass('hide-letter');
                });
            }

            $.post("{{ route('game.letter') }}", {letter: letter, found: found.length})
                .done(function (data) {
                    if (data.status === 'success' && found.length > 0) {
                        $('.notification').addClass('notification-success').removeClass('notification-fail').text(`Udało Ci się zgadnąć literę. Ilość odkrytych liter: ${found.length}.`);
                        sounds.good.play();
                    } else if (data.status === 'duplicate') {
                        $('.notification').addClass('notification-fail').removeClass('notification-success').text('Już użyłeś tej litery.');
                        sounds.fail.play();
                    } else if (data.status === 'endGame') {
                        $("#exampleModalCenter").modal();
                        sounds.good.play();
                    } else {
                        $('.notification').addClass('notification-fail').removeClass('notification-success').text('Wybrana litera nie należy do hasła.');
                        sounds.fail.play();
                    }
                });
        });

        $('.theme-switch').click(function () {
            let currentTheme = $('body').attr('class').trim();
            $('body').attr('class', '');
            if (currentTheme === 'theme-dark') {
                currentTheme = 'theme-light';
                $('body').addClass('theme-light');
                $(this).text('Ciemny motyw');
            } else {
                currentTheme = 'theme-dark';
                $('body').addClass('theme-dark');
                $(this).text('Jasny motyw');
            }
            $.post( "{{ route('game.theme') }}", { theme: currentTheme});
        });

        $('.force-win').click(function (e) {
            $.post( "{{ route('game.win') }}", { win:  true }).done(function (data) {
                if (data.status === 'success') {
                    let found = $('.game-table').find('.has-letter');
                    found.each(function () {
                        $(this).removeClass('hide-letter');
                    });
                }
            });
        });
    })
</script>
</body>
</html>