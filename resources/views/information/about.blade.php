@extends('layouts.information')

@section('content')
        <div class="card">
            <h5 class="card-title text-warning shadow-sm pb-3">O projekcie</h5>
            <div class="card-body">
                <span class="a"><h5>
            Koło fortuny - gra wzorowana na podstawie programu Wheel of fortune.
                        Napisana hobbistycznie, głownie w celu edukacyjnym.</h5>
                </span>
                <div class="authors ">
                <h4>Twórcy:</h4>
                    <div class="flex-container">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold ">Marek</h4>
                            <h6 class="card-subtitle mb-2 text-muted"><span class="badge badge-warning">CEO</span></h6>
                            <p class="card-text">.... </p>
                            <a href="#" class="card-link">Link do profilu</a>
                        </div>
                    </div>
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h4 class="card-title font-weight-bold">Fabian</h4>
                                <h6 class="card-subtitle mb-2 text-muted"><span class="badge badge-info">FRONTEND</span></h6>
                                <p class="card-text">.... </p>
                                <a href="#" class="card-link">Link do profilu</a>
                            </div>
                        </div>
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h4 class="card-title font-weight-bold ">Sylwester</h4>
                                <h6 class="card-subtitle mb-2 text-muted"><span class="badge badge-info">BACKEND</span></h6>
                                <p class="card-text">.... </p>
                                <a href="#" class="card-link">Link do profilu</a>
                            </div>
                        </div>

            </div>
        </div>

    </div>
    <div class="flex-item-team">
        <div></div>
    </div>
</div>
@endsection