@extends('layouts.information')

@section('content')
    <div class="card">
        <h5 class="card-title text-warning shadow-sm pb-3">Przeczytaj zanim zaczniesz!</h5>
        <span class="card-body">
                <span class="a"><h5>
                        Gra wzoruje się na oryginalnej grze <b>Wheel Of Fortune</b>.<br/>
            W obecnym stanie aplikacja dostosowana jest pod grę z użyciem własnego koła lub innego przedmiotu losującego wraz z osoba podsumowująca wynik gry.<br/>
            Dodanie systemu losowania oraz koła przewidujemy w następnych aktualizacjach.<br/>
                </h5></span>
            <h3 class="mt-5">
                Wykorzystane źródła:
            </h3>
            <span>Dźwięki :<br/>
                <a href="http://soundbible.com/1127-Computer-Error.html">Computer Error Sound</a> Recorded by Mike Koenig</span><br/>
                 <a href="https://www.soundjay.com/failure-sound-effect.html">Failure Sound Effects</a> From soundjay</span>
        <div class="flex-item-team">
            <div></div>
        </div>
    </div>
@endsection