<!doctype html>
<html lang="en">
<head>
    <!-- CSRF token-->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <title>{{ config('app.name') }}</title>
</head>
<body class="theme-dark">

<nav class="navbar navbar-expand-lg navbar-dark navbar-custom">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name') }}</a>

    </div>
    <button type="button" class="navbar-toggler ml-auto float-right" data-toggle="collapse" data-target="#mynavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="mynavbar">
        <ul class="navbar-nav ml-auto">

            @auth
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('sentence.index') }}">
                        <button type="button" data-toggle="tooltip" data-placement="bottom" title="Panel gry"
                                class="btn btn-outline-warning"><i class="fas fa-user-alt fa-lg"></i></button>
                    </a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">
                        <button type="button" class="btn btn-outline-warning">Zaloguj</button>
                    </a>

                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">
                            <button type="button" class="btn btn-outline-warning">Rejestracja</button>
                        </a>
                    </li>
                @endif
            @endauth
        </ul>
    </div>
</nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
